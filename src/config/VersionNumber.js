function getVersionNumber() {
  if (__DEV__) {
    const {version} = require('../../package.json');
    return {
      appVersion: version,
      buildVersion: new Date().getTime().toString(),
      bundleIdentifier: 'com.poc_react_native_dimed',
    };
  }

  return require('react-native-version-number');
}

module.exports = getVersionNumber();
