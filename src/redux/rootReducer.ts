import { combineReducers } from 'redux';
import newGithubReducer from 'redux-app/features/github/GithubSlice';
import reducer from 'redux-app/reducers';

const rootReducer = combineReducers({
  newGithub: newGithubReducer,
  github: reducer,
});

export default rootReducer;
