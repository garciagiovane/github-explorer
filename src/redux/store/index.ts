import { createStore } from 'redux';
import rootReducer from 'redux-app/rootReducer';

const store = createStore(rootReducer);

export default store;
