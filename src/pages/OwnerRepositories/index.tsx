import React, {useEffect, useState} from 'react';
import {FlatList, View, ListRenderItemInfo} from 'react-native';
import RepoOwner from 'types/RepoOwner';
import {getRepos} from 'services/features/github/GithubService';
import Repository from 'types/Repository';
import Icon from 'react-native-vector-icons/Feather';
import {
  RepositorieInfo,
  RepositoriesContainer,
  RepositoriesDescription,
  RepositoriesName,
  RepositoriesAvatar,
} from 'pages/Dashboard/styles';
import {useNavigation, RouteProp} from '@react-navigation/native';

type RootStackParamList = {
  Repositories: RepoOwner;
};

type OwnerRepositoriesRouteProp = RouteProp<RootStackParamList, 'Repositories'>;

type Props = {
  route: OwnerRepositoriesRouteProp;
};

const OwnerRepositories: React.FC<Props> = ({route}: Props) => {
  const {repoOwner} = route.params;
  const [repos, setRepos] = useState<Repository[] | null>(null);
  const navigation = useNavigation();

  useEffect(() => {
    getRepos(repoOwner.login).then((completion) => {
      setRepos(completion);
    });
  }, [repoOwner.login]);

  const handleOnPress = (repository: string) => {
    navigation.navigate('Details', {
      repository,
    });
  };

  const renderItem: React.FC<ListRenderItemInfo<Repository>> = (
    info: ListRenderItemInfo<Repository>,
  ) => {
    const {item, index} = info;
    return (
      <RepositoriesContainer
        onPress={() => handleOnPress(item.full_name)}
        key={index}>
        <RepositoriesAvatar
          source={{
            uri: repoOwner.avatar_url,
          }}
        />
        <RepositorieInfo>
          <RepositoriesName>{item.full_name}</RepositoriesName>
          <RepositoriesDescription>{item.description}</RepositoriesDescription>
        </RepositorieInfo>
        <Icon name="chevron-right" size={36} color="#39B100" />
      </RepositoriesContainer>
    );
  };

  return (
    <View>
      {repos && (
        <FlatList
          keyExtractor={(item, _) => item.full_name.toString()}
          data={repos}
          renderItem={renderItem}
        />
      )}
    </View>
  );
};

export default OwnerRepositories;
