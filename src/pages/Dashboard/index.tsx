import React, { useState } from 'react';
import { KeyboardAvoidingView, ScrollView, Platform } from 'react-native';
import { useDispatch } from 'react-redux';
import Input from 'components/Input';
import { getOwnerDetails } from 'services/features/github/GithubService';
import RepoOwner from 'types/RepoOwner';
import RepoOwnerBox from 'pages/Dashboard/RepoOwnerBox';
import {
  Container,
  Title,
  MessageError,
  Button,
  ButtonText,
} from 'pages/Dashboard/styles';
import { useNavigation } from '@react-navigation/native';

export interface Repository {
  full_name: string;
  description: string;
  owner: {
    login: string;
    avatar_url: string;
  };
}

interface objError {
  message: string;
  isError: boolean;
}

const Dashboard: React.FC = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const [repoOwner, setRepoOwner] = useState<RepoOwner>();
  const [searchValue, setSearchValue] = useState('');
  const [inputError, setInputError] = useState<objError>({
    message: '',
    isError: false,
  });

  const handleOwnerButtonPress = () => {
    dispatch({
      type: 'github/add',
      payload: repoOwner,
    });
    navigation.navigate('Repositories', {
      repoOwner,
    });
  };

  const handleAddRepositories = () => {
    if (searchValue) getOwnerDetails(searchValue, setInputError, setRepoOwner);
  };

  return (
    <KeyboardAvoidingView
      style={{ flex: 1 }}
      behavior={Platform.select({
        ios: 'padding',
        default: undefined,
      })}
      enabled>
      <ScrollView
        keyboardShouldPersistTaps="handled"
        contentContainerStyle={{ flex: 1 }}>
        <Container>
          <Title>Explore repositórios no GitHub</Title>

          <Input
            name="userRepo"
            placeholderTextColor="#B7B7CC"
            value={searchValue}
            placeholder="Digite o usuário"
            onChangeText={(value) => setSearchValue(value)}
            error={inputError.isError}
            testID="user-repo"
          />
          <MessageError>{inputError.message}</MessageError>

          <Button testID={'button'} onPress={handleAddRepositories}>
            <ButtonText>Adicionar</ButtonText>
          </Button>

          {repoOwner && (
            <RepoOwnerBox
              repoOwner={repoOwner}
              onPress={handleOwnerButtonPress}
            />
          )}
        </Container>
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

export default Dashboard;
