import styled from 'styled-components/native';
import Colors from '../../constants/Colors';

export const TextTitle = styled.Text`
  font-size: 26px;
  text-align: center;
  padding-vertical: 10px;
  font-family: 'Poppins-Regular';
`;

export const TextDescription = styled.Text`
  font-size: 16px;
  text-align: center;
  padding-vertical: 5px;
  padding-horizontal: 5px;
  font-family: 'Poppins-Regular';
`;

export const Version = styled.Text`
  font-family: 'Poppins-Regular';
  color: ${Colors.taupeGrayColor};
  align-self: center;
  padding-vertical: 40px;
  font-size: 12px;
`;
