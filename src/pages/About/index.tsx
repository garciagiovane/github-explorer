import React from 'react';
import {ImageBackground, View} from 'react-native';
import imgAbout from '../../assets/about.png';

import Layout from '../../constants/Layout';
import VersionNumber from '../../config/VersionNumber';

import {TextTitle, TextDescription, Version} from './styles';

const About: React.FC = () => {
  return (
    <View>
      <ImageBackground
        source={imgAbout}
        style={{width: Layout.windowWidth, height: Layout.normalize(280)}}
        resizeMode="cover"
      />
      <TextTitle>Sobre o GitHub Explorer</TextTitle>
      <TextDescription>
        Aplicação simples para demonstração e aplicação de códigos em
        ReactNative, e TypeScript, StyledComponents, e Redux.
      </TextDescription>
      <Version>{`v${VersionNumber.appVersion} (${VersionNumber.buildVersion})`}</Version>
    </View>
  );
};

export default About;
