import React from 'react';
import styled from 'styled-components';
import {View, Image, Animated, Text} from 'react-native';
import {Layout, Images, Colors} from '../constants';
import VersionNumber from '../config/VersionNumber';

const Container = styled(Animated.View)`
  width: ${Layout.windowWidth}px;
  height: ${Layout.windowHeight}px;
  align-items: center;
  justify-content: center;
  background-color: white;
`;

const PreviousImage = styled(Image)`
  width: 280px;
  resize-mode: contain;
  position: absolute;
`;

const NewImage = styled(PreviousImage)`
  position: relative;
`;

const Version = styled(Text)`
  position: absolute;
  bottom: 10px;
  font-family: 'Poppins-Regular';
  color: ${Colors.taupeGrayColor};
  align-self: center;
  padding-vertical: 40px;
  font-size: 13px;
`;

const SplashScreen: React.FC = ({animation, children}: any) => {
  return (
    <Container>
      <PreviousImage source={Images.icon} />
      <View style={{width: 280, position: 'absolute'}}>
        <Animated.View
          style={{
            width: animation.interpolate({
              inputRange: [0, 1],
              outputRange: [0, 280],
            }),
            overflow: 'hidden',
          }}>
          <NewImage
            source={Images.icon}
            style={{tintColor: 'rgb(33, 131, 74)'}}
          />
          {children}
        </Animated.View>
      </View>
      <Version>{`v${VersionNumber.appVersion} (${VersionNumber.buildVersion})`}</Version>
    </Container>
  );
};

export default SplashScreen;
