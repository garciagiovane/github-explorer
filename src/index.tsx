import 'react-native-gesture-handler';
import React, {useEffect, useState} from 'react';
import {View, StatusBar, Animated} from 'react-native';
import SplashScreenController from 'react-native-splash-screen';
import {Provider} from 'react-redux';
import Routes from './routes';
import store from './redux/store';
import SplashScreen from './components/SplashScreen';

const App: React.FC = () => {
  const [splashScreenVisibility, setSplashScreenVisibility] = useState(true);
  const splashAnimationValue = new Animated.Value(0);

  useEffect(() => {
    SplashScreenController.hide();
    Animated.timing(splashAnimationValue, {
      toValue: 1,
      duration: 900,
      useNativeDriver: false,
    }).start(() => setSplashScreenVisibility(false));
    // SplashScreen.hide();
  }, [splashAnimationValue]);

  if (splashScreenVisibility) {
    return <SplashScreen animation={splashAnimationValue} />;
  }

  return (
    <Provider store={store}>
      <View style={{backgroundColor: '#312e38', flex: 1}}>
        <StatusBar
          barStyle="light-content"
          backgroundColor="transparent"
          translucent
        />
        <Routes />
      </View>
    </Provider>
  );
};

export default App;
