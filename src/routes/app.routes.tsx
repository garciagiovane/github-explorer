import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { Link, NavigationContainer } from '@react-navigation/native';
import { StyleSheet, Image, Text, View } from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import OwnerRepositories from 'pages/OwnerRepositories';
import Dashboard from 'pages/Dashboard';
import Details from 'pages/Details';
import Profile from 'pages/Profile';
import Logo from 'assets/img/logo.png';
import About from 'pages/About';

const App = createStackNavigator();

const CustomHeader = () => (
  <View style={styles.header}>
    <Image source={Logo} />
    <Link to="/About">
      <Icon name="info" size={36} color="#39B100" />
    </Link>
  </View>
);

const AppRoutes: React.FC = () => (
  <NavigationContainer>
    <App.Navigator
      screenOptions={{
        headerShown: true,
        cardStyle: { backgroundColor: '#EBEEF8' },
      }}
      initialRouteName="Dashboard">
      <App.Screen
        options={{
          headerTitle: () => <CustomHeader />,
        }}
        name="Dashboard"
        component={Dashboard}
      />
      <App.Screen
        options={{
          headerTitleAlign: 'center',
          headerTitleStyle: { alignSelf: 'center', justifyContent: 'center' },
          headerTitle: () => <Image source={Logo} />,
          headerBackTitleVisible: false,
          headerLeftContainerStyle: {
            marginLeft: 20,
          },
        }}
        name="Details"
        component={Details}
      />
      <App.Screen
        options={{
          headerTitle: () => (
            <Text
              style={{
                fontFamily: 'Poppins-Regular',
                fontSize: 18,
              }}>
              Issue
            </Text>
          ),
          headerTitleAlign: 'center',
          headerTitleStyle: {
            alignSelf: 'center',
            justifyContent: 'center',
          },
        }}
        name="Profile"
        component={Profile}
      />
      <App.Screen
        options={{
          headerTitleAlign: 'center',
          headerTitleStyle: { alignSelf: 'center', justifyContent: 'center' },
          headerTitle: () => <Image source={Logo} />,
        }}
        name="About"
        component={About}
      />
      <App.Screen
        options={{
          headerTitle: () => (
            <Text
              style={{
                fontFamily: 'Poppins-Regular',
                fontSize: 18,
              }}>
              Repositórios
            </Text>
          ),
          headerTitleAlign: 'center',
          headerTitleStyle: {
            alignSelf: 'center',
            justifyContent: 'center',
          },
        }}
        name="Repositories"
        component={OwnerRepositories}
      />
    </App.Navigator>
  </NavigationContainer>
);

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    // display: 'flex',
    // alignItems: 'center',
    // width: 120,
    // backgroundColor: '#fc0',
    // flex: 1,
    // alignSelf: 'stretch',
  },
  title: {
    fontSize: 20,
    color: 'blue',
    fontWeight: 'bold',
  },
  subtitle: {
    fontSize: 16,
    color: 'purple',
    fontWeight: 'bold',
  },
});

export default AppRoutes;
