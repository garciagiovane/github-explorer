/* eslint-disable max-len */
import Layout from './Layout';

const CATEGORIES_SLIDER_WIDTH = Layout.windowWidth;
const CATEGORIES_SLIDER_HEIGHT =
  Layout.windowHeight -
  (Layout.headerHeight + Layout.lgGutter) * 2 -
  Layout.iphoneXIndicatorHeight;
const CATEGORIES_CAROUSEL_MARGIN_TOP =
  Layout.headerHeight - Layout.lgGutter + Layout.statusBarHeight;

export default {
  CATEGORIES_CAROUSEL_MARGIN_TOP,
  CATEGORIES_SLIDER_HEIGHT,
  CATEGORIES_SLIDER_WIDTH,
};
