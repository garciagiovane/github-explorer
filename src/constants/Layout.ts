import {Dimensions, Platform, PixelRatio} from 'react-native';
import theme from 'styled-theming';
import {getStatusBarHeight, getIPhoneXIndicatorHeight} from '../utils/Device';
import Colors from './Colors';

const {width, height} = Dimensions.get('window');

const getScale = (smallScreenScaling = 0.9, limitScale = true) => {
  let scale = height / 640;
  if (scale > 1 && limitScale) {
    scale = 1;
  } else if (scale < 1) {
    scale *= smallScreenScaling;
  }
  return scale;
};

const getNormalize = (size) => {
  const scale = width / 360;

  const newSize = size * scale;
  return Math.round(PixelRatio.roundToNearestPixel(newSize));
};

export default {
  windowWidth: width,
  windowHeight: height,
  hitSlop: {top: 10, left: 10, right: 10, bottom: 10},
  smShadow:
    Platform.OS === 'ios' &&
    `
    shadow-color: ${Colors.primaryColor};
    shadow-offset: 0;
    shadow-opacity: 0.12;
    shadow-radius: 8;
  `,
  testShadow:
    Platform.OS === 'ios' &&
    `
    shadow-color: ${Colors.primaryColor};
    shadow-offset: 0;
    shadow-opacity: 1;
    shadow-radius: 8;
  `,
  mdShadow:
    Platform.OS === 'ios' &&
    `
    shadow-color: ${Colors.primaryColor};
    shadow-offset: 0;
    shadow-opacity: 0.18;
    shadow-radius: 8;
  `,
  theme: (light, dark, currentTheme) => {
    if (currentTheme) {
      return currentTheme.mode === 'light' || currentTheme === 'light'
        ? light
        : dark;
    }
    return theme('mode', {
      light,
      dark,
    });
  },
  largeHitSlop: {top: 15, left: 15, right: 15, bottom: 15},
  statusBarHeight: getStatusBarHeight() ?? 0,
  iphoneXIndicatorHeight: getIPhoneXIndicatorHeight(),
  isSmallDevice: width < 375,
  xsGutter: 4,
  smGutter: 8,
  mdGutter: 16,
  lgGutter: 24,
  xlGutter: 32,
  xxlGutter: 48,
  headerHeight: 56,
  tabBarHeight: 56,
  scale: getScale,
  normalize: getNormalize,
};
