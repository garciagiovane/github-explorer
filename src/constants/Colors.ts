const darkBlueColor = '#0f2350';
const primaryBlueColor = '#183d8e';
const secondaryBlueColor = '#01bfdf';
const weakSecondaryBlueColor = '#3dcee6';
const tertiaryBlueColor = '#748bbb';
const primaryIceBlueColor = '#eff4fd';
const paperColor = '#ffffff';
const primaryGrayColor = '#f8f8f8';
const secondaryGrayColor = '#b2b2b2';
const taupeGrayColor = '#909090';
const tertiaryGrayColor = '#666666';
const primaryGreenColor = '#1fab89';
const lightGreenColor = '#BFD731';
const secondaryGreenColor = '#0eb7b4';
const primaryOrangeColor = '#F66715';
const secondaryOrangeColor = '#f78305';
const primaryBrownColor = '#9d7864';
const secondaryBrownColor = '#5e3b11';
const primaryRedColor = '#da1d52';
const secondaryRedColor = '#db5353';
const primaryPinkColor = '#fad4e1';
const warningColor = '#faa619';
const displayMakeupColor = 'rgba(0, 0, 0, 0.48)';
const appleGreenColor = '#9db40e';
const blackFridayYellow = '#FEF701';
const blackFridayBlack = '#000000';
const blackFridayBadgeBackground = '#231f20';

export default {
  darkBlueColor,
  primaryColor: primaryBlueColor,
  secondaryColor: secondaryBlueColor,
  tertiaryColor: tertiaryBlueColor,
  primaryIceBlueColor,
  paperColor,
  primaryBlueColor,
  secondaryBlueColor,
  weakSecondaryBlueColor,
  primaryGrayColor,
  secondaryGrayColor,
  taupeGrayColor,
  primaryGreenColor,
  lightGreenColor,
  secondaryGreenColor,
  primaryOrangeColor,
  secondaryOrangeColor,
  primaryBrownColor,
  secondaryBrownColor,
  primaryRedColor,
  secondaryRedColor,
  primaryPinkColor,
  displayMakeupColor,
  textGray: tertiaryGrayColor,
  backgroundColor: '#fbfbfb',
  statusBarColor: '#2d1c66',
  activeTintColor: secondaryBlueColor,
  inactiveTintColor: '#b2b2b2',
  tabBarBottom: {
    backgroundColor: paperColor,
  },
  warningColor,
  appleGreenColor,
  blackFridayYellow,
  blackFridayBlack,
  blackFridayBadgeBackground,
};
