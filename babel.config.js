module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      'module-resolver',
      {
        root: ['./src'],
        alias: {
          services: './src/services',
          pages: './src/pages',
          components: './src/components',
          assets: './src/assets',
          config: './src/config',
          constants: './src/constants',
          'redux-app': './src/redux',
          routes: './src/routes',
          utils: './src/utils',
        },
      },
    ],
  ],
};
